# BDAg 1 - Web Bots

## Objetivo 
A fim de expandir o conhecimento, criamos este projeto que visa centralizar todo o conteúdo gerado para a coleta de dados com Web Bots.
Vale ressaltar que a coleta de dados é o primeiro passo para ser inserido neste contexto de Big Data. Web Bot é apenas um destes meios.

## Linguagem Usada
Python

## Bibliotecas Disponíveis neste projeto

1 - Scrapy
* Exemplo: scrapy/projects/spiders/na_invasao.py

2 - Html Request
* Exemplo: bot_html_request.py
    
## Observações
Posteriormente estaremos enriquecendo este readme incluindo uns passo-a-passo, tutorial, etc.

Se alguém se sentir a vontade em compartilhar o conhecimento aqui, fique à vontade.

Obrigado,
Fernando S. Maransatto