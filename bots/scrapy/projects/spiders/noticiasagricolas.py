#-*- coding: utf-8 -*-
# encoding: iso-8859-1
# encoding: win-1252
import scrapy
import pymongo
from scrapy.conf import settings
from datetime import datetime
from projects.items import Noticias
import time

class NaInvasaoSpider(scrapy.Spider):
    name = 'noticiasagricolas'
    cidades = ['Pompéia']

    def start_requests(self):
        urls = [
                    'https://www.noticiasagricolas.com.br/pesquisa/?q=invas%E3o', # não encontrado em pompeia
                    'https://www.noticiasagricolas.com.br/pesquisa/?q=assalto', # não encontrado em pompeia
                    'https://www.noticiasagricolas.com.br/pesquisa/?q=roubo', # não encontrado em pompeia
                    'https://www.noticiasagricolas.com.br/pesquisa/?q=pol%EDcia'
                    'http://g1.globo.com/busca/?q=roubo+fazenda+gado',
                    'http://g1.globo.com/busca/?q=invas%C3%B5es+mst',
                    'http://g1.globo.com/busca/?q=assalto+fazenda'
        ]

        # Loop à partir dos links principais
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    # Define o nome do Contexto conforme parte da URL principal
    def contexto(self, url):
        return {
            'q=invas%E3o'          : 'Invasão',
            'q=assalto'            : 'Assalto',
            'q=roubo'              : 'Roubo',
            'q=pol%EDcia'          : 'Polícia',
            'q=roubo+fazenda+gado' : 'Roubo de Gado',
            'q=invas%C3%B5es+mst'  : 'Invasão',
            'q=assalto+fazenda'    : 'Assalto'
        }[url]

    def parse(self, response):

        if 'g1.globo.com' in response.url:

            for href in response.xpath('//*[@id="documentos"]/ul/li/div/div/a').extract():
                request = scrapy.Request(response.urljoin(href),
                                         callback=self.parse_g1_redireciona,
                                         dont_filter=True)
                request.meta['url_original'] = response.url
                yield request

            #Segue para a próxima página, na busca
            next_page = response.xpath('//*[@id="paginador"]/div/ul/li[7]/a/@href').extract_first()
            if next_page is not None:
                next_page = response.urljoin(next_page)
                yield scrapy.Request(next_page, callback=self.parse)

        elif 'noticiasagricolas.com' in response.url:

            for href in response.xpath('//*[@id="resultados-busca"]/ul/li/a/@href').extract():
                request = scrapy.Request(response.urljoin(href),
                                         callback=self.parse_noticiasagricolas,
                                         dont_filter=True) # Lucas, incluido dont_filter aqui
                request.meta['url_original'] = response.url
                yield request

            #Segue para a próxima página, na busca
            next_page = response.xpath('//*[@id="resultados-busca"]/div/ul/li[8]/a/@href').extract_first()
            if next_page is not None:
                next_page = response.urljoin(next_page)
                yield scrapy.Request(next_page, callback=self.parse)

    def parse_noticiasagricolas(self, response):

        #seleciona a data de publicação e atualização.
        texto_da_data = response.xpath('//*[contains(@class, "datas")]/text()').extract_first()

        # anos para retornar as notícias
        anos = ['2019']

        if any(ano in texto_da_data for ano in anos):

            #separa as datas do texto "publicado em xx/xx/xxxx e atualizado em xx/xx/xxxx"
            str_data_publicacao = texto_da_data[17:27]
            str_data_atualizacao = texto_da_data[55:65]

            data_atualizacao = None

            data_publicacao = datetime.combine(datetime.strptime(str_data_publicacao, '%d/%m/%Y').date(), datetime.min.time())
            if str_data_atualizacao:
                data_atualizacao = datetime.combine(datetime.strptime(str_data_atualizacao, '%d/%m/%Y').date(), datetime.min.time())

            #por questão de performance, só recupera o título e o conteúdo se a data estiver conforme condição acima
            lista_conteudo = response.xpath('//*[contains(@class, "materia")]/p/text()').extract()
            conteudo = u''.join(lista_conteudo).encode('utf-8').strip()

            # Tem que usar um objeto de item.py para que seja possível enviar para o banco lá em pipelines.py
            item = Noticias()
            item['cidade_regiao'] = cidade_regiao
            item['url'] = response.url
            item['contexto'] = self.contexto(response.request.meta['url_original'].split('?')[1])
            item['site'] = 'www.noticiasagricolas.com.br'
            item['titulo'] = response.xpath('//*[contains(@class, "page-title")]/text()').extract_first()
            item['conteudo'] = conteudo
            item['data_publicacao'] = data_publicacao
            item['data_atualizacao'] = data_atualizacao

            yield item

    def parse_g1_redireciona(self, response):

        globo_eu_te_odeio_toma_essa_por_querer_escapar_do_meu_spider = response.url.split('u=')[1].split('&amp;')[0].replace('%3A', ':').replace('%2F', '/')

        request = scrapy.Request(url=globo_eu_te_odeio_toma_essa_por_querer_escapar_do_meu_spider,
                                 callback=self.parse_g1,
                                 dont_filter=True)
        request.meta['url_original'] = response.url
        yield request

    def parse_g1(self, response):


        data_publicacao = response.xpath('//*[@itemprop="datePublished"]/@datetime').extract_first()
        data_atualizacao = response.xpath('//*[@itemprop="dateModified"]/@datetime').extract_first()

        # anos para retornar as notícias
        anos = ['2019']

        if any(ano in str(data_publicacao) for ano in anos) or any(ano in str(data_atualizacao) for ano in anos):

            titulo = response.xpath('//*[@class="content-head__title"]/text()').extract_first()
            subtitulo = response.xpath('//*[@class="content-head__subtitle"]/text()').extract_first()
            criadopor = response.xpath('//*[@class="content-meta-info__from"]/span/text()').extract_first()
            conteudo = 'u'.join(response.xpath('//*[@data-block-type="unstyled"]/p/text()').extract()).encode('utf-8').strip()

            # Tem que usar um objeto de item.py para que seja possível enviar para o banco lá em pipelines.py
            item = Noticias()
            item['cidade_regiao'] = cidade_regiao
            item['url'] = response.url
            item['contexto'] = self.contexto(response.request.meta['url_original'].split('?')[1].split('&amp;')[0])
            item['site'] = 'g1.globo.com'
            item['titulo'] = titulo
            item['subtitulo'] = subtitulo
            item['conteudo'] = conteudo
            item['criadopor'] = criadopor
            item['data_publicacao'] = data_publicacao
            item['data_atualizacao'] = data_atualizacao

            yield item
